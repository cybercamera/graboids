extends Area2D

onready var ClimbSound = preload("res://assets/sounds/stairs.ogg")

var climb_sound
var player_on_roof = false
var player_body 

func _ready():
	climb_sound = AudioStreamPlayer.new()

func ClimbUp():
	climb_sound.set_stream(ClimbSound) #Use the same sound stream as de-materialise sound
	climb_sound.set_bus("master")
	climb_sound.set_name("climb_sound")
	add_child(climb_sound)
	climb_sound.play()
	player_body.visible = false
	player_body.position = $StandingPoint.global_position
	player_body.SetIdleAnimation()
	player_on_roof = true
	$ClimbTimer.start()
		

func _on_Building3_body_entered(body):
	#Check if body entering is a player, in which case shift them onto the StandingPoint after some movement noise
	print("_on_Building1_body_entered(body): " + str(body))
	if (body.get_name() == "Player"):
		player_body = body
		ClimbUp()


func _on_Building3_body_exited(body):
	if player_on_roof:
		#player was already on the roof, so she has to jump off. 
		body.PlayerJump($Sprite.texture.get_size().y * scale.y) #Send the texture height


func _on_ClimbTimer_timeout():
	player_body.visible = true
	climb_sound.stop()
	
