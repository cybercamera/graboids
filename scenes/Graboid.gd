extends Area2D

export var min_speed = 50
export var max_speed = 200
var speed = 100
var velocity = Vector2()
var direction = Vector2()
var screensize

func _ready():
	randomize()
	screensize = get_viewport_rect().size
	$MovementTimer.start()
	

func _process(delta):
	position += velocity * delta
	position.x = clamp(position.x, 0, screensize.x)
	position.y = clamp(position.y, 0, screensize.y)

func emit_dust() -> void:
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a graboid dust particle and attach it to the parent level 
		var graboid_dust_scene = load("res://scenes/GraboidDust.tscn")
		var graboid_dust_puff = graboid_dust_scene.instance()
		get_parent().add_child(graboid_dust_puff) #Add to level node
		graboid_dust_puff.position = global_position

func emit_sand() -> void:
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a graboid dust particle and attach it to the parent level 
		var graboid_sand_scene = load("res://scenes/GraboidBodySand.tscn")
		var graboid_sand_puff = graboid_sand_scene.instance()
		get_parent().add_child(graboid_sand_puff) #Add to level node
		graboid_sand_puff.position = global_position

func _on_MovementTimer_timeout():
	speed = randi()%150 + 50
	direction.x = rand_range(-1,1)
	direction.y = rand_range(-1,1)
	velocity = direction.normalized() * speed
	rotation = rad2deg(atan2(direction.normalized().y, direction.normalized().x) - PI/2)
	$BodySandTimer.start()

func _on_BodySandTimer_timeout():
	emit_sand()
	emit_dust()
	
