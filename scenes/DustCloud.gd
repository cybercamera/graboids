extends Node2D

enum WALK_PACE {tiptoe = 1, walk = 2, run = 3}
enum DUST_DIRECTION {up, down, left, right}
export(DUST_DIRECTION) var dust_direction = DUST_DIRECTION.right


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func ChangeDirection(dir):
	match dir:
		DUST_DIRECTION.right:
			$Particles2D.rotation = 0
			self.position = Vector2(14,25)
		DUST_DIRECTION.left:
			$Particles2D.rotation = 180
			self.position = Vector2(-14,25)
		DUST_DIRECTION.up:
			$Particles2D.rotation = 270
			self.position = Vector2(0,25)
		DUST_DIRECTION.down:
			$Particles2D.rotation = 90
			self.position = Vector2(0,30)

func ChangePace(walk_pace):
	#print("ChangePace(walk_pace): " + str(walk_pace))
	walk_pace = int(abs(walk_pace))
	#print("ChangePace(walk_pace): " + str(walk_pace))
	#print ("typeof: " + typeof(walk_pace))
	match walk_pace:
		WALK_PACE.tiptoe :
			$Particles2D.amount = 2
			$Particles2D.lifetime = 1
			$Particles2D.process_material.set_param(ParticlesMaterial.PARAM_INITIAL_LINEAR_VELOCITY, 5)
		WALK_PACE.walk:
			$Particles2D.amount = 30
			$Particles2D.lifetime = 2
			$Particles2D.process_material.set_param(ParticlesMaterial.PARAM_INITIAL_LINEAR_VELOCITY, 10)
		WALK_PACE.run:
			$Particles2D.amount = 60
			$Particles2D.lifetime = 3
			$Particles2D.process_material.set_param(ParticlesMaterial.PARAM_INITIAL_LINEAR_VELOCITY, 20)
			
	#print("amount: " + str($Particles2D.amount) + " lifetime: " + str($Particles2D.lifetime) + " vel: " + str($Particles2D.process_material.get_param(ParticlesMaterial.PARAM_INITIAL_LINEAR_VELOCITY)))
