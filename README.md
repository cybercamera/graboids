# Graboids

You're a geologist investigating strange seismic activity and find yourself in a precarious situation of cat-and-mouse survival, with dangerous creatures which move through and attack from below the ground as the predators.

Outline of game
---------------

- Top down arcade game.
- You play an animated 2d character. 
- You move by clicking to where you want to move to by the left mouse button, or running to there by clicking the right mouse button.
- Your opponents are underground worm-monsters ("graboids") which aren't visible onscreen most of the time, but when they move, they 'shift' some of the sand above themselves as they're tunneling around.
- The graboids have no sight, so only track by sound. If you keep still (i.d, don't move) then they can't feel the vibrations through the earth.
- The graboids move fast, far faster than you can run. Trying to outrun them is futile, unless you have a strategy of escaping them in place.
- You can escape the graboids by climbing onto large solid objects, like boulders or you temporarily onto houses or cars. Eventually, they will dig you out from under a car or a house however.
- You can collect objects along the way. These objects you can throw by holding the Control key and clicking to where you want to throw something to. If you're clicking much further than you can throw, the throw will only reach as far as you can throw an object of that size/weight.
- Thrown objects can be used as 'decoys' in the cat-and-mouse game with the graboids. 
- Sometimes, you will find dynamite, which you can throw. When it explodes, it creates enough of a painful vibration that it scares the grabods away for a few minutes, allowing you to make a run for it. If you don't throw it far away enough, you could bring about your own demise.
- Because you're in the desert, you'll need to find regular sources for water and for food. Over time, you'll run low on both. Failing to find sufficient water and food will also bring about your demise. 
- The oject of the game is to get to a location from which you can contact help. 

Additional ideas
----------------

- There are 3 modes of play. One is to run and escape, The other is endurance survival. The third is to kill all the graboids.
- There are old mines around and old mining camps. These sometimes have water, food and dynamite. You can collect/consume the water and food, and collect the dynamite for blow graboids up.
- There are health bars for food and water. If these run out, you die within a limited time-span. (~2 mins?)


Game Mechanics
--------------

- Syncing footsteps with sound: https://www.youtube.com/watch?v=XZLfl8p4riQ

