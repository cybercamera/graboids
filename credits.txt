Information
-----------

https://www.youtube.com/watch?v=j2gJgFzg-yU&list=PLsk-HSGFjnaEVtp4pR-N0RGt-iNmvSH5h&index=3&t=0s <- tilemap animation with shaders

Tilesets
--------
https://opengameart.org/content/rpg-desert-tileset
https://opengameart.org/content/free-desert-top-down-tileset
https://opengameart.org/content/desert-ground-textures

Procedural tilemap generator: https://www.youtube.com/watch?v=XESdOh3Arsg


Spritesheets
------------
https://opengameart.org/content/lpc-sara
https://opengameart.org/content/stella
https://opengameart.org/content/lpc-combat-armor-for-women
https://opengameart.org/content/animated-nature-assets
http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/

Images
------
https://opengameart.org/content/recursive-cave-entrance

Sounds
------

https://opengameart.org/content/fantozzis-footsteps-grasssand-stone
https://opengameart.org/content/stone-stair-steps

